//
//  CordinatorView.swift
//  NewsFeed
//
//  Created by user on 27.09.2023.
//

import SwiftUI

struct CordinatorView: View {
    @StateObject var cordinator = Cordinator()
    var body: some View {
        NavigationStack(path: $cordinator.path){
            cordinator.buildScreen(route: .home)
                .navigationDestination(for: ScreenRoute.self){ route in
                    cordinator.buildScreen(route: route)
                }
                .sheet(item: $cordinator.shet){ shet in
                    cordinator.buildShet(route: shet)
                }
        }
        .environmentObject(cordinator)
    }
}

