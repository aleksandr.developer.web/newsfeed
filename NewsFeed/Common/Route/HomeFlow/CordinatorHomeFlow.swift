//
//  Cordinator.swift
//  NewsFeed
//
//  Created by user on 27.09.2023.
//

import SwiftUI

enum ScreenRoute: Identifiable, Hashable {
    
    static func == (lhs: ScreenRoute, rhs: ScreenRoute) -> Bool {
        return true
    }
    
    case home
    case second
    case secondParam(text: String)
    case details
    case detailsParam(news: Article)
    
    var id: String {
        switch self {
        case .home:
            return "home"
        case .second, .secondParam:
            return "second"
        case .details, .detailsParam:
            return "details"
        }
    }
    
    func hash(into hasher: inout Hasher) {
        switch self {
        case .home:
            hasher.combine("home")
        case .second:
            hasher.combine("second")
        case .secondParam(let text):
            hasher.combine("secondParam")
            hasher.combine(text)
        case .details:
            hasher.combine("details")
        case .detailsParam(let news):
            hasher.combine("detailsParam")
            hasher.combine(news.title)
        }
    }
}

enum ShetRoute: Hashable, Identifiable{
    static func == (lhs: ShetRoute, rhs: ShetRoute) -> Bool {
        return true
    }
    
    case comment(postId: Int)
    case auth
    case searchMenu
    
    var id: String{
        switch self{
        case .comment:
            return "comment"
        case .auth:
            return "auth"
        case .searchMenu:
            return "searchMenu"
        }
}

func hash(into hasher: inout Hasher) {
    switch self{
    case .comment:
        hasher.combine("comment")
    case .auth:
        hasher.combine("auth")
    case .searchMenu:
        hasher.combine("searchMenu")
    }
}
}

protocol CordinatorProtocol {
    func push(route: ScreenRoute)
    func pop()
    func popToRoute()
    func presendShet(shet: ShetRoute)
    func disposeShet()
}

class Cordinator: ObservableObject, CordinatorProtocol {
    
    @Published var path = NavigationPath()
    @Published var shet: ShetRoute?
    
    
    func push(route: ScreenRoute) {
        self.path.append(route)
    }
    
    func pop() {
        self.path.removeLast()
    }
    
    func popToRoute() {
        self.path.removeLast(path.count)
    }
    
    func presendShet(shet: ShetRoute) {
        self.shet = shet
    }
    
    func disposeShet() {
        self.shet = nil
    }
    
    
    @ViewBuilder
    func buildScreen(route: ScreenRoute) -> some View {
        switch route {
        case .home:
            HomeView(homeController: HomeController(repository: CategoryNewsRepository()))
        case.second:
            SecondView(title: "")
        case .secondParam(text: let text):
            SecondView(title: text)
        case .details:
            DetailsView()
        case .detailsParam(news: let news):
            DetailsView(news: news)
        }
    }
    
    @ViewBuilder
    func buildShet(route: ShetRoute) -> some View {
        switch route {
        case .comment(let postId):
            ComentShetView(postId: postId)
        case .auth:
            AuthShetView()
        case .searchMenu:
            SearchMenuView(homeController: HomeController(repository: CategoryNewsRepository()))
        }
    }
}

