//
//  AuthController.swift
//  NewsFeed
//
//  Created by user on 26.09.2023.
//

import Foundation

class AuthController: ObservableObject{
    @Published var title: String
    
    init(){
        self.title = ""
    }
    
    func changedTitle(value: String) {
        self.title = value
    }
    
    func clearTitle(){
        self.title = ""
    }
}
