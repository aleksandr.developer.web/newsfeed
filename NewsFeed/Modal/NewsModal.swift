//
//  CategoryNews.swift
//  NewsFeed
//
//  Created by user on 24.01.2024.
//

import Foundation

class Article: Codable {
    let source: Source?
    let author, title, description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    let content: String?

    init(source: Source?, author: String?, title: String?, description: String?, url: String?, urlToImage: String?, publishedAt: String?, content: String?) {
        self.source = source
        self.author = author
        self.title = title
        self.description = description
        self.url = url
        self.urlToImage = urlToImage
        self.publishedAt = publishedAt
        self.content = content
    }
}


class Source: Codable, Identifiable  {
    let id, name: String?

    init(id: String?, name: String?) {
        self.id = id
        self.name = name
    }
}
