//
//  ResponseModal.swift
//  NewsFeed
//
//  Created by user on 24.01.2024.
//

import Foundation

class ResponseModal: Codable {
    let status: String?
    let totalResults: Double?
    let articles: [Article]?

    init(status: String?, totalResults: Double?, articles: [Article]?) {
        self.status = status
        self.totalResults = totalResults
        self.articles = articles
    }
}
