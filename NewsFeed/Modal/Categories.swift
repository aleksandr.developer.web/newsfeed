//
//  Categories.swift
//  NewsFeed
//
//  Created by user on 24.01.2024.
//

import Foundation

enum Categories: String {
    case Business
    case Entertainment
    case General
    case Health
    case Science
    case Sports
    case Technology
}
