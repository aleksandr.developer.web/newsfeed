//
//  NewsFeedApp.swift
//  NewsFeed
//
//  Created by user on 20.09.2023.
//

import SwiftUI

@main
struct NewsFeedApp: App {
    var body: some Scene {
        WindowGroup {
            CordinatorView()
        }
    }
}
