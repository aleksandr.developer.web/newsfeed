//
//  CustomTapBarView.swift
//  NewsFeed
//
//  Created by user on 27.09.2023.
//

import SwiftUI

struct CustomTapBarView: View {
    var body: some View {
        VStack{
//            Spacer()
            HStack(alignment: .center){
                VStack{
                    Image("Home")
                }
                .onTapGesture {
                    
                }
                Spacer()
                VStack{
                    Image("News")
                }
                .onTapGesture {
                    
                }
                Spacer()
                VStack{
                    Image("Profile")
                }
                .onTapGesture {
                    
                }
                
            }
            .padding(.horizontal, 42)
            .frame(height: 80.0)
            .background(Color("GrayOne"))
        }
    }
}
