//
//  ApiServise.swift
//  NewsFeed
//
//  Created by user on 10.10.2023.
//

import Foundation


class Service: ApiClient {
    
    func fetchPostsFunc() async throws -> [PostModal]{
        do {
            let response = try await fetchPosts()
            return response
        } catch {
            return []
        }
    }
}
