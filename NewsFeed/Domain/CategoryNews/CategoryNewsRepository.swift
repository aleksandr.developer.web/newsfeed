//
//  CategoryNewsRepository.swift
//  NewsFeed
//
//  Created by user on 24.01.2024.
//

import Foundation

protocol CategoryNewsRepositoryProtocol{
    func fetchCategoryNews(categories: String) async -> [Article]?
}

struct CategoryNewsRepository: CategoryNewsRepositoryProtocol {
    var listA: [Article]? = []
    
    func fetchCategoryNews(categories: String) async -> [Article]? {
        
        CategoryNewsDatasource.shared.fetchCategoryNewsJustList(categories: categories){ data in
            _ = data
        }
        
        CategoryNewsDatasource.shared.fetchCategoryNewsResult(categories: categories) { data in
            do{
                _ = try data.get()
            } catch {
                
            }
        }
        
        do{
            let listA = try await  CategoryNewsDatasource.shared.fetchCategoryNewsUrlSession(categories: categories).get()
            return listA
        } catch{
            
        }
        
        
        return listA
    }
}
