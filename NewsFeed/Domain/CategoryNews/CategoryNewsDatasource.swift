//
//  CategoryNewsDatasource.swift
//  NewsFeed
//
//  Created by user on 24.01.2024.
//

import Foundation
import Alamofire

protocol CategoryNewsDatasourceProtocol{
    func fetchCategoryNewsResult(categories: String, completion: @escaping (Result<[Article]?, Error>) -> ())
    func fetchCategoryNewsJustList(categories: String, completion: @escaping ([Article]?) -> ())
    func fetchCategoryNewsUrlSession(categories: String) async throws -> Result<[Article]?, Error>
}

struct CategoryNewsDatasource: CategoryNewsDatasourceProtocol {
    static let shared = CategoryNewsDatasource()
    
    func fetchCategoryNewsResult(categories: String, completion: @escaping (Result<[Article]?, Error>) -> ()) {
        
        func createUrl() -> String {
            let apiKey = "apiKey=\(apiKey)"
            let country = "&country=ru"
            let language = "&language=ru"
            let category = "&category=\(categories)"
            let url = baseUrl + apiKey + language + country + category
            
            return url
        }
        
        AF.request(createUrl(), method: .get)
            .response { response in
                
                guard let data = response.data else { 
                    completion(.failure(NetworkError.notData))
                    return }
                                
                do{
                    let decoder = JSONDecoder()
                    let result = try decoder.decode(ResponseModal.self, from: data)
                    let listArticle = result.articles
                    
                    completion(.success(listArticle))
                } catch {
                    completion(.failure(NetworkError.notCanotParse))
                }
            }
    }
    
    func fetchCategoryNewsJustList(categories: String, completion: @escaping ([Article]?) -> ()) {
        
        AF.request("\(baseUrl)category=\("sport")&apiKey=\(apiKey)", method: .get)
            .validate()
            .response { response in
                
                guard let data = response.data else { return }
                
                do{
                    let decoder = JSONDecoder()
                    let result = try decoder.decode(ResponseModal.self, from: data)
                    let listArticle = result.articles
                    
                    completion(listArticle)
                } catch let error {
                    completion([])
                }
            }
    }
    
    func fetchCategoryNewsUrlSession(categories: String) async throws -> Result<[Article]?, Error> {
        guard let url = URL(string: "\(baseUrl)&category=\(categories)") else { throw CError.errorName }
        
        let (data, response) = try await URLSession.shared.data(from: url)
        
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else { throw CError.errorName }
        
        let decode = JSONDecoder()
        
        do{
            let result = try decode.decode(ResponseModal.self, from: data)
            
            return .success(result.articles)
        } catch let error {
            return .failure(error)
        }
        
        
    }
}

enum NetworkError: String, Error {
    case notData = "Нет даты"
    case notCanotParse = "Парсинг провалился"
    case badStatusCode = "Статус код не 200"
}



