//
//  ApiServise.swift
//  NewsFeed
//
//  Created by user on 26.09.2023.
//

import Foundation

protocol ApiServiseProtocol {
    func fetchPosts() async throws -> [PostModal]
}

class ApiClient: ApiServiseProtocol {
    func fetchPosts() async throws -> [PostModal] {
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/posts") else { throw CError.errorName }
        
        let (data, response) = try await URLSession.shared.data(from: url)
        
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else { throw CError.errorName }
        
        do{
            let decode = JSONDecoder()
            let result = try decode.decode([PostModal].self, from: data)
            return result
        } catch {
            return []
        }
    }
    
        
    func client<T: Decodable>(for: T.Type, url: String) async throws -> T? {
        
        guard let url = URL(string: "\(baseUrl)\(url)") else { throw CError.errorName }
        
        let (data, response) = try await URLSession.shared.data(from: url)
        
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else { throw CError.errorName }
        
        do{
            let decode = JSONDecoder()
            let result = try decode.decode(T.self, from: data)
            return result
        } catch {
            return nil
        }
    }
}

enum CError: Error {
    case errorName
}
