//
//  SearchMenu.swift
//  NewsFeed
//
//  Created by user on 25.01.2024.
//

import SwiftUI

struct SearchMenuView: View {
    let homeController: HomeController
    @EnvironmentObject var cordinator: Cordinator
    
    var body: some View {
        
        VStack{
            
            Button("Бизнес"){
                Task{
                    await homeController.updateList(newCategories: "business")
                    cordinator.disposeShet()
                }
             }
            .padding(EdgeInsets(top: 0.0, leading: 0.0, bottom: 20.0, trailing: 0.0))
            
            Button("Развлечения"){
                Task{
                    await homeController.updateList(newCategories: "entertainment")
                    cordinator.disposeShet()
                }
             }
            .padding(EdgeInsets(top: 0.0, leading: 0.0, bottom: 20.0, trailing: 0.0))
            
            Button("Главные"){
                Task{
                    await homeController.updateList(newCategories: "general")
                    cordinator.disposeShet()
                }
             }
            .padding(EdgeInsets(top: 0.0, leading: 0.0, bottom: 20.0, trailing: 0.0))
            
            Button("Спорт"){
                Task{
                    await homeController.updateList(newCategories: "sports")
                    cordinator.disposeShet()
                }
               
            }
            .padding(EdgeInsets(top: 0.0, leading: 0.0, bottom: 20.0, trailing: 0.0))
            
            Button("Технологии"){
                Task{
                    await homeController.updateList(newCategories: "technology")
                    cordinator.disposeShet()
                }
                
            }
            .padding(EdgeInsets(top: 0.0, leading: 0.0, bottom: 20.0, trailing: 0.0))
            
        }
        
    }
}

