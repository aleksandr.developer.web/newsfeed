//
//  ComentShetView.swift
//  NewsFeed
//
//  Created by user on 27.09.2023.
//

import SwiftUI

struct ComentShetView: View {
    @EnvironmentObject var cordinator: Cordinator
    var postId: Int
    var body: some View {
        Text("Это id поста \(postId)")
            .padding()
        Button(action: {
            cordinator.disposeShet()
        }){
            Text("Close")
        }
    }
}


