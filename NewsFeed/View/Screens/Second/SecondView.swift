
import SwiftUI

struct SecondView: View {
    @EnvironmentObject var authController: AuthController
    @EnvironmentObject var cordinator: Cordinator
    @State private var titleField = ""
    var title: String
    var body: some View {
        NavigationView{
            VStack{
                Text(title)
                    .fontWeight(.bold)
                Spacer()
                Button(action: {
                    cordinator.popToRoute()
                }){
                    Text("Go back home")
                }
            }
            .navigationBarTitle("SecondView", displayMode: .automatic)
        }
    }
}

