//
//  HomeNewCardListView.swift
//  NewsFeed
//
//  Created by user on 25.01.2024.
//

import SwiftUI

struct HomeNewCardListView: View {
    let homeController: HomeController
    let cordinator: Cordinator
    var listNews: [Article]?
    
    var body: some View {
        if homeController.isLoading {
            ProgressView()
        } else {
            ScrollView(.horizontal, showsIndicators: false){
                LazyHStack{
                    ForEach(listNews ?? [], id: \.title){ item in
                        NewsCard(news: item)
                            .listRowInsets(EdgeInsets())
                            .listRowBackground(Color.clear)
                            .padding(10)
                            .onTapGesture {
                                cordinator.push(route: .detailsParam(news: item))
                            }
                    }
                }
            }
            .padding(.top, 16.0)
        }
    }
}

 
