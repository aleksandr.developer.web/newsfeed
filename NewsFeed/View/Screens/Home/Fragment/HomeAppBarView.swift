//
//  HomeAppBarView.swift
//  NewsFeed
//
//  Created by user on 27.09.2023.
//

import SwiftUI

struct HomeAppBarView: View {
    var body: some View {
        VStack{
            HStack{
                Image("Logo")
                    .resizable()
                    .frame(width: 35, height: 35)
                Text("NewsFeed")
                    .foregroundColor(Color("White"))
                    .font(.system(size: 30))
                    .padding(.leading, 10)
                Spacer()
                Image("Notification")
                    .frame(width: 35, height: 35)
                    .background(Color("GrayTwo"))
                    .cornerRadius(100.0)
            }
            .padding(.horizontal, 16)
             
        }
        
    }
}
 
