//
//  HomeInfoView.swift
//  NewsFeed
//
//  Created by user on 25.01.2024.
//

import SwiftUI

struct HomeInfoView: View {
    let cordinator: Cordinator
    @State private var searchText = ""
    var body: some View {
        VStack(alignment: .leading){
            Text("googMoarning")
                .foregroundColor(Color(.white))
                .font(.system(size: 18))
            Text("Discover Breaking News")
                .foregroundColor(Color(.white))
                .font(.system(size: 30))
                .fontWeight(.bold)
            HStack{
                TextField("Search", text: $searchText)
                    .padding()
                    .frame(height: 50)
                    .background(Color(.grayTwo))
                    .cornerRadius(12)
                Image("Filter")
                    .frame(width: 50.0, height: 50.0)
                    .background(Color(.grayTwo))
                    .cornerRadius(12)
                    .onTapGesture {
                        cordinator.presendShet(shet: .searchMenu)
                    }
            }
            
        }
        .padding(.horizontal, 16.0)
        .padding(.vertical, 20.0)
    }
}


