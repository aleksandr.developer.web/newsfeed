//
//  NewsCardView.swift
//  NewsFeed
//
//  Created by user on 27.09.2023.
//

import SwiftUI

struct NewsCard: View {
    var news: Article
    @EnvironmentObject var cordinator: Cordinator
    
    var body: some View {
        
        VStack(alignment: .leading){
            AsyncImage(url: URL(string: "https://www.financialounge.com/wp-content/uploads/2016/05/picks-16-5-16.jpg")) { img in
                img
                    .resizable()
                    .frame(width: 225, height: 120)
                    .aspectRatio(contentMode: .fit)
                    
            } placeholder: {
                ProgressView()
            }
            .frame(width: 225, height: 120)
            .cornerRadius(15)
            Text(news.title ?? "")
                .fontWeight(.bold)
            Text(news.description ?? "")
            Spacer()
            Text(String(news.author ?? ""))
                .foregroundColor(Color(.white))
                .padding(.horizontal, 8)
                .padding(.vertical, 5)
                .background(Color(.black))
                .cornerRadius(5)
            Spacer()
        }
        .padding(15)
        .frame(width: 250, height: 280)
        .background(Color(.white))
        .cornerRadius(20)
    }
}
