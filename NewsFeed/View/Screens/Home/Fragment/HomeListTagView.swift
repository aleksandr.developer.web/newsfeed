//
//  HomeListTagView.swift
//  NewsFeed
//
//  Created by user on 25.01.2024.
//

import SwiftUI

struct HomeListTagView: View {
    let homeController: HomeController
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false){
            HStack{
                Spacer(minLength: 16.0)
                Button("Бизнес"){
                    Task{
                        await homeController.updateList(newCategories: "business")
                    }
                }
                .buttonStyle(.borderedProminent)
                .padding(.trailing, 6.0)
                
                Button("Развлечения"){
                    Task{
                        await homeController.updateList(newCategories: "entertainment")
                    } 
                }
                .buttonStyle(.borderedProminent)
                .padding(.trailing, 6.0)
                
                Button("Главные"){
                    Task{
                        await homeController.updateList(newCategories: "general")
                    }
                }
                .buttonStyle(.borderedProminent)
                .padding(.trailing, 6.0)
                
                Button("Спорт"){
                    Task{
                        await homeController.updateList(newCategories: "sports")
                    }
                }
                .buttonStyle(.borderedProminent)
                .padding(.trailing, 6.0)
                
                Button("Технологии"){
                    Task{
                        await homeController.updateList(newCategories: "technology")
                    }
                }
                .buttonStyle(.borderedProminent)
                .padding(.trailing, 20.0)
            }
        }
    }
}
