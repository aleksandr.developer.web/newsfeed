//
//  HomeController.swift
//  NewsFeed
//
//  Created by user on 26.09.2023.
//
//
import Foundation

class HomeController: ObservableObject {
    var repository: CategoryNewsRepository
    @Published var listNews: [Article]? = []
    @Published var categories = "business"
    @Published var isLoading = false
    
    let endpoint = "\(baseUrl)todos"
    
    init(repository: CategoryNewsRepository){
        self.repository = repository
        self.listNews = []
    }
    
    func fetchNews() {
        self.isLoading = true
        CategoryNewsDatasource.shared.fetchCategoryNewsResult(categories: categories) { result in
            
            switch result{
            case .success(let data):
                self.listNews = data
            case .failure(let error):
                print("Ошибка при получение новостей = \(error)")
            }
        }
        self.isLoading = false
    }
    
    func updateList(newCategories: String) async {
        self.isLoading = true
        self.categories = newCategories
        CategoryNewsDatasource.shared.fetchCategoryNewsResult(categories: categories) { data in
            do{
                self.listNews = try data.get()
            } catch {
                
            }
        }
        self.isLoading = false
    }
}
