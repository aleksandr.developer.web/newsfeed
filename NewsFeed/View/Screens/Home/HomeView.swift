//
//  ContentView.swift
//  NewsFeed
//
//  Created by user on 20.09.2023.
//

import SwiftUI

struct HomeView: View {
    @ObservedObject var homeController: HomeController
    @EnvironmentObject var cordinator: Cordinator
    
    init(homeController: HomeController){
        print("set state in home view")
        self.homeController = homeController
        Task {
            await homeController.fetchNews()
        }
    }
    
    var body: some View {
        NavigationView(){
            VStack(alignment: .leading){
                HomeAppBarView()
                
                ScrollView(.vertical){
                    HomeInfoView(cordinator: cordinator)
                    HomeListTagView(homeController: homeController)
                    HomeNewCardListView(homeController: homeController, cordinator: cordinator, listNews: homeController.listNews)
                }
                
                CustomTapBarView()
            }
            .padding(.top, 70)
            .background(VStack(spacing: .zero) { Color("Primary"); Color("GrayTwo") })
            .ignoresSafeArea(.container, edges: .all)
        }
    }
}


